//
//  RegistrationViewModel.swift
//  Created by Evgeny Lee on 15.11.17.
//

import RxSwift

class RegistrationViewModel: BaseViewModel {
    
    var name = Variable("")
    var surname = Variable("")
    var dateOfBirth = Variable("")
    var sexIsMale = Variable(false)
    var email = Variable("")
    var password = Variable("")
    var rePpassword = Variable("")
    
    var didSelectSex = Variable(false)
    var isAgree = Variable(false)
    var didSignUp = Variable(false)
    var isValidData = Variable("")
    
    var isValidEmail: Observable<Bool> {
        return self.email.asObservable().map { value -> Bool in
            return value.isValidEmail
        }
    }
    
    var isValidPassword: Observable<Bool> {
        return self.password.asObservable().map { value -> Bool in
            return value.isValidPassword
        }
    }
    
    var isValidRePassword: Observable<Bool> {
        return self.rePpassword.asObservable().map {[weak self] value -> Bool in
            return value.isValidPassword && (self?.password.value == self?.rePpassword.value)
        }
    }
    
    private var isValidFormData: Observable<String> {
        let isValidName = name.asObservable().map { value -> Bool in
            return !value.isEmpty
        }
        
        let isValidSurname = surname.asObservable().map { value -> Bool in
            return !value.isEmpty
        }
        
        let isValidDate = dateOfBirth.asObservable().map { value -> Bool in
            return !value.isEmpty
        }
        
        return Observable.combineLatest(isValidName, isValidSurname, didSelectSex.asObservable(), isValidEmail, isValidPassword, isValidRePassword, isValidDate, isAgree.asObservable()) { hasName, hasSurname, hasSex, hasEmail, hasPassword, hasRePassword, hasDate, didAgree in
            if !hasName {
                return L10n.Register.Error.name
            }
            
            if !hasSurname {
                return L10n.Register.Error.surname
            }
            
            if !hasSex {
                return L10n.Register.Error.sex
            }
            
            if !hasEmail {
                return L10n.Register.Error.email
            }
            
            if !hasPassword {
                return L10n.Register.Error.password
            }
            
            if !hasRePassword {
                return L10n.Register.Error.rePassword
            }
            
            if !hasDate {
                return L10n.Register.Error.dateOfBirth
            }
            
            if !didAgree {
                return L10n.Register.Error.agreement
            }
            
            return ""
        }
    }
    
    override init() {
        super.init()
        isValidFormData.bind(to: isValidData).disposed(by: disposeBag)
    }
    
    func signUp() {
        isLoading.value = true
        
        let form = RegistrationForm(name: name.value,
                                    surname: surname.value,
                                    dateOfBirth: dateOfBirth.value,
                                    sex: sexIsMale.value ? .male : .female,
                                    email: email.value,
                                    password: password.value,
                                    passwordConfirmation: rePpassword.value)
        
        BackendController.shared.signUp(form).subscribe(onNext: {[weak self] _ in
            self?.isLoading.value = false
            self?.didSignUp.value = true
        }, onError: {[weak self] error in
            self?.handleError(error)
        }).disposed(by: disposeBag)
    }
    
    override func handleError(_ error: Error) {
        isLoading.value = false
        if let backendError = error as? BackendError {
            if backendError.isAppError {
                let errorMessage = backendError.signUpErrorMessage()
                if errorMessage.isEmpty {
                    appErrorMessage.value = error.localizedDescription
                } else {
                    appErrorMessage.value = errorMessage
                }
                return
            }
        }
        sysErrorMessage.value = error.localizedDescription
    }
}
