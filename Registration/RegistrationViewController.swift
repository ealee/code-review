//
//  RegistrationViewController.swift
//  Created by Evgeny Lee on 15.11.17.
//

import RxSwift
import RxCocoa
import FacebookCore
import FacebookLogin
import DateToolsSwift

class RegistrationViewController: BaseViewController {
    @IBOutlet weak private var textLabel: UILabel!
    @IBOutlet weak private var nameTextField: CustomTextField!
    @IBOutlet weak private var surnameTextField: CustomTextField!
    @IBOutlet weak private var maleView: UIView!
    @IBOutlet weak private var maleCheckBox: CheckBoxView!
    @IBOutlet weak private var maleLabel: UILabel!
    @IBOutlet weak private var femaleView: UIView!
    @IBOutlet weak private var femaleCheckBox: CheckBoxView!
    @IBOutlet weak private var femaleLabel: UILabel!
    @IBOutlet weak private var emailTextField: CustomTextField!
    @IBOutlet weak private var passwordTextField: CustomTextField!
    @IBOutlet weak private var rePasswordTextField: CustomTextField!
    @IBOutlet weak private var dateTextField: CustomTextField!
    @IBOutlet weak private var agreeCheckBox: CheckBoxView!
    @IBOutlet weak private var agreeLabel: UILabel!
    @IBOutlet weak private var signUpButton: CustomButton!
    
    let viewModel = RegistrationViewModel()
    var textFields = [UITextField]()
    
    // MARK: - Instance
    
    class var controller: RegistrationViewController {
        return StoryboardScene.Auth.registrationViewController.instantiate()
    }
    
    // MARK: - View lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        localizeView()
        configureView()
        bindViewModel()
    }
    
    // MARK: - Private
    
    private func localizeView() {
        textLabel.text = L10n.Register.Label.text
        nameTextField.placeholder = L10n.Auth.Placeholder.firstName
        surnameTextField.placeholder = L10n.Auth.Placeholder.lastName
        maleLabel.text = L10n.Register.Label.male
        femaleLabel.text = L10n.Register.Label.female
        emailTextField.placeholder = L10n.Auth.Placeholder.email
        passwordTextField.placeholder = L10n.Auth.Placeholder.password
        rePasswordTextField.placeholder = L10n.Auth.Placeholder.confirmPassword
        dateTextField.placeholder = L10n.Auth.Placeholder.dateOfBirth
        agreeLabel.attributedText = L10n.Register.Label.agreeText.styled(with: L10n.Register.Label.agreeTextToHighlight, underlined: true)
        signUpButton.setTitle(L10n.Register.Button.create, for: .normal)
    }
    
    private func configureView() {
        configureTextFields()
        
        maleView.rx.tapGesture.bind(onNext: {[weak self] view in
            self?.updateSexRadioBox(view)
        }).disposed(by: disposeBag)
        
        femaleView.rx.tapGesture.bind(onNext: {[weak self] view in
            self?.updateSexRadioBox(view)
        }).disposed(by: disposeBag)
        
        agreeCheckBox.rx.tapGesture.bind(onNext: {[weak self] _ in
            self?.updateAgreeCheckBox()
        }).disposed(by: disposeBag)
        
        agreeLabel.rx.tapGesture.bind(onNext: {[weak self] _ in
            self?.showTermsOfService()
        }).disposed(by: disposeBag)
        
        signUpButton.rx.tap.bind(onNext: {[weak self] _ in
            self?.signUp()
        }).disposed(by: disposeBag)
    }
    
    private func bindViewModel() {
        baseBind(viewModel: viewModel, disposeBag: disposeBag)
        
        bindTextFields()
        
        viewModel.isValidEmail.bind(onNext: {[weak self] value in
            self?.emailTextField.isValidText = value
        }).disposed(by: disposeBag)
        
        viewModel.isValidPassword.bind(onNext: {[weak self] value in
            self?.passwordTextField.isValidText = value
        }).disposed(by: disposeBag)
        
        viewModel.isValidRePassword.bind(onNext: {[weak self] value in
            self?.rePasswordTextField.isValidText = value
        }).disposed(by: disposeBag)
        
        viewModel.didSignUp.asObservable().bind(onNext: {[weak self] value in
            if value {
                self?.showRegistrationConfirm()
            }
        }).disposed(by: disposeBag)
    }
    
    private func configureTextFields() {
        textFields = [nameTextField, surnameTextField, emailTextField, passwordTextField, rePasswordTextField, dateTextField]
        for textField in textFields {
            textField.delegate = self
            if let tField = textField as? CustomTextField {
                tField.placeholderColor = .steelGrey
            }
        }
        
        emailTextField.rx.textValue.bind(onNext: {[weak self] textValue in
            self?.emailTextField.text = textValue.filteredEmail
        }).disposed(by: disposeBag)
        
        let passwordButton = passwordTextField.createRightButton(image: Asset.Auth.viewPasswordOff.image)
        passwordButton.rx.tap.bind(onNext: {[weak self] _ in
            if let weakSelf = self {
                weakSelf.togglePasswordVisibility(textField: weakSelf.passwordTextField)
            }
        }).disposed(by: disposeBag)
        
        let rePasswordButton = rePasswordTextField.createRightButton(image: Asset.Auth.viewPasswordOff.image)
        rePasswordButton.rx.tap.bind(onNext: {[weak self] _ in
            if let weakSelf = self {
                weakSelf.togglePasswordVisibility(textField: weakSelf.rePasswordTextField)
            }
        }).disposed(by: disposeBag)
        
        let dateButton = dateTextField.createRightButton(image: Asset.Auth.calendar.image)
        dateButton.rx.tap.bind(onNext: {[weak self] _ in
            self?.dateTextField.becomeFirstResponder()
        }).disposed(by: disposeBag)
        
        let datePicker = UIDatePicker()
        datePicker.tintColor = .white
        datePicker.datePickerMode = .date
        datePicker.maximumDate = Date() - TimeChunk(seconds: 0, minutes: 0, hours: 0, days: 0, weeks: 0, months: 0, years: 14)
        datePicker.rx.date.skip(1).bind(onNext: {[weak self] date in
            self?.updateDate(date)
        }).disposed(by: disposeBag)
        dateTextField.inputView = datePicker
    }
    
    private func bindTextFields() {
        nameTextField.rx.textValue.bind(onNext: {[weak self] textValue in
            let text = textValue.filteredName
            self?.nameTextField.text = text
            self?.viewModel.name.value = text
        }).disposed(by: disposeBag)
        
        surnameTextField.rx.textValue.bind(onNext: {[weak self] textValue in
            let text = textValue.filteredName
            self?.surnameTextField.text = text
            self?.viewModel.surname.value = text
        }).disposed(by: disposeBag)
        
        passwordTextField.rx.textValue.bind(onNext: {[weak self] textValue in
            let text = textValue.filteredPassword
            self?.passwordTextField.text = text
            self?.viewModel.password.value = text
        }).disposed(by: disposeBag)
        
        rePasswordTextField.rx.textValue.bind(onNext: {[weak self] textValue in
            let text = textValue.filteredPassword
            self?.rePasswordTextField.text = text
            self?.viewModel.rePpassword.value = text
        }).disposed(by: disposeBag)
    }
    
    private func updateSexRadioBox(_ view: UIView) {
        guard let checkBox = view == maleView ? maleCheckBox : femaleCheckBox else {
            return
        }
        
        if checkBox.isChecked {
            return
        }
        
        checkBox.isChecked = !checkBox.isChecked
        if checkBox == maleCheckBox {
            femaleCheckBox.isChecked = !checkBox.isChecked
        } else {
            maleCheckBox.isChecked = !checkBox.isChecked
        }
        maleLabel.textColor = maleCheckBox.isChecked ? UIColor.paleGrey : UIColor.steelGrey
        femaleLabel.textColor = femaleCheckBox.isChecked ? UIColor.paleGrey : UIColor.steelGrey
        
        viewModel.sexIsMale.value = maleCheckBox.isChecked
        viewModel.didSelectSex.value = true
    }
    
    private func togglePasswordVisibility(textField: CustomTextField) {
        textField.isSecureTextEntry = !textField.isSecureTextEntry
        if let button = textField.rightView as? UIButton {
            let image = textField.isSecureTextEntry ? Asset.Auth.viewPasswordOff.image : Asset.Auth.viewPasswordOn.image
            button.setImage(image, for: .normal)
        }
    }
    
    private func updateDate(_ date: Date) {
        dateTextField.text = date.longString()
        viewModel.dateOfBirth.value = date.digitsString()
    }
    
    private func updateAgreeCheckBox() {
        agreeCheckBox.isChecked = !agreeCheckBox.isChecked
        viewModel.isAgree.value = agreeCheckBox.isChecked
    }
    
    private func showTermsOfService() {
        ViewManager.shared.showTermsOfService(delegate: self)
    }
    
    private func showRegistrationConfirm() {
        ViewManager.shared.showRegistrationConfirm(email: viewModel.email.value)
    }
    
    private func signUp() {
        view.endEditing(true)
        if viewModel.isValidData.value.isEmpty {
            viewModel.signUp()
        } else {
            showAlertWithError(viewModel.isValidData.value)
        }
    }
}

extension RegistrationViewController: UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        switch textField {
        case emailTextField:
            emailTextField.isValidText = true
        default:
            return
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        switch textField {
        case nameTextField:
            var text = ""
            if let strippedText = nameTextField.text?.strippedBeginEndWhiteSpaces {
                text = strippedText
            }
            nameTextField.text = text
            viewModel.name.value = text
            
        case surnameTextField:
            var text = ""
            if let strippedText = surnameTextField.text?.strippedBeginEndWhiteSpaces {
                text = strippedText
            }
            surnameTextField.text = text
            viewModel.surname.value = text
            
        case emailTextField:
            viewModel.email.value = emailTextField.text ?? ""
            
        default:
            return
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField.returnKeyType == .next {
            if let index = textFields.index(of: textField) {
                let nextTextField = textFields[index + 1]
                nextTextField.becomeFirstResponder()
                return false
            }
        }
        
        view.endEditing(true)
        return true
    }
}

extension RegistrationViewController: TermsViewControllerDelegate {
    func didAcceptTerms() {
        if !agreeCheckBox.isChecked {
            updateAgreeCheckBox()
        }
    }
}
